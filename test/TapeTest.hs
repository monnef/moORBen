{-# OPTIONS_GHC -F -pgmF htfpp #-}

module TapeTest where

import           Control.Arrow
import           Data.Either                   (isLeft)
import           Data.Function
import           Flow
import           Test.Framework
import           Text.ParserCombinators.Parsec hiding (spaces)

import           Parser.MoorbenParser
import           Runtime.RuntimeState

test_ensureTapeIndexIsValid :: IO ()
test_ensureTapeIndexIsValid = do
  assertEqual (Tapes 0 [TapeStack []]) startingTapes

  let tapes1 = ensureTapeIndexIsValid startingTapes 3
  let tapes1e = Tapes 0 [TapeStack [], TapeStack [], TapeStack [], TapeStack []]
  assertEqual tapes1e tapes1

  let tapes2 = ensureTapeIndexIsValid startingTapes (-2)
  let tapes2e = Tapes (-2) [TapeStack [], TapeStack [], TapeStack []]
  assertEqual tapes2e tapes2

  let tapes3 = Tapes 0 [TapeStack [StackBool True]] & \x->ensureTapeIndexIsValid x (-2) & \x->ensureTapeIndexIsValid x 1
  let tapes3e = Tapes (-2) [TapeStack [], TapeStack [], TapeStack [StackBool True], TapeStack []]
  assertEqual tapes3e tapes3

sbt = StackBool True
si1 = StackInt 1
scA = StackChar 'A'

test_pushToTape :: IO ()
test_pushToTape = do
  let tape1 = Tapes 0 [TapeStack []]

  assertEqual (Tapes 0 [TapeStack [sbt]]) $ pushToTape tape1 0 sbt

  let tape2 = Tapes 1 [TapeStack []]
  assertEqual (Tapes 1 [TapeStack [sbt]]) $ pushToTape tape2 1 sbt

  let tape3 = Tapes 1 [TapeStack [si1],TapeStack [sbt]]
  let tape3e = Tapes 1 [TapeStack [si1],TapeStack [scA, sbt]]
  assertEqual tape3e $ pushToTape tape3 2 scA

  let tape4 = Tapes 0 [TapeStack []]
  let tape4e = Tapes 0 [TapeStack [StackChar 'A', StackChar 'B']]
  assertEqual tape4e $ pushStringToTape tape4 0 "AB"


test_popFromTape :: IO ()
test_popFromTape = do
  let exp1 = (Nothing, Tapes 0 [TapeStack []])
  let tape1 = Tapes 0 [TapeStack []]
  assertEqual exp1 $ popFromTape tape1 0

  let exp2 = (Just sbt, Tapes 0 [TapeStack []])
  let tape2 = Tapes 0 [TapeStack [sbt]]
  assertEqual exp2 $ popFromTape tape2 0

  let exp3 = (Just sbt, Tapes 0 [TapeStack [scA]])
  let tape3 = Tapes 0 [TapeStack [sbt, scA]]
  assertEqual exp3 $ popFromTape tape3 0

  let exp4 = (Just sbt, Tapes 0 [TapeStack [scA], TapeStack[si1]])
  let tape4 = Tapes 0 [TapeStack [scA], TapeStack [sbt, si1]]
  assertEqual exp4 $ popFromTape tape4 1

  let exp5 = (Just sbt, Tapes (-1) [TapeStack [scA], TapeStack[si1]])
  let tape5 = Tapes (-1) [TapeStack [scA], TapeStack [sbt, si1]]
  assertEqual exp5 $ popFromTape tape5 0

test_popNFromTape :: IO ()
test_popNFromTape = do
  let sbt = StackBool True
  let si1 = StackInt 1
  let scA = StackChar 'A'

  let exp1 = ([], Tapes 0 [TapeStack []])
  let tape1 = Tapes 0 [TapeStack []]
  assertEqual exp1 $ popNFromTape tape1 0 0

  let exp2 = ([sbt, si1, scA], Tapes 0 [TapeStack []])
  let tape2 = Tapes 0 [TapeStack [sbt, si1, scA]]
  assertEqual exp2 $ popNFromTape tape2 0 3

  let exp3 = ([sbt, si1], Tapes 0 [TapeStack [scA]])
  let tape3 = Tapes 0 [TapeStack [sbt, si1, scA]]
  assertEqual exp3 $ popNFromTape tape3 0 2

test_pushListToTape :: IO ()
test_pushListToTape = do
  let sbt = StackBool True
  let si1 = StackInt 1
  let si2 = StackInt 2
  let scA = StackChar 'A'

  let exp1 = Tapes 0 [TapeStack [sbt, si1, scA, si2]]
  let tape1 = Tapes 0 [TapeStack [si2]]
  assertEqual exp1 $ pushListToTape tape1 0 [scA, si1, sbt]
