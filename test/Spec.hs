{-# OPTIONS_GHC -F -pgmF htfpp #-}
{-# LANGUAGE FlexibleContexts #-}

module Main where

import           Test.Framework

import {-@ HTF_TESTS @-} ParserTest
import {-@ HTF_TESTS @-} TapeTest

main :: IO ()
main = htfMain htf_importedTests
