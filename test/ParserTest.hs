{-# OPTIONS_GHC -F -pgmF htfpp #-}

module ParserTest where

import           Control.Arrow
import           Data.Either                   (isLeft)
import           Data.Function
import           Flow
import           Test.Framework
import           Text.ParserCombinators.Parsec hiding (spaces)

import           Parser.MoorbenParser
import           Runtime.RuntimeState

posNegCase :: Parser a -> [String -> IO ()]
posNegCase parser = [positive, negative]
  where
    fullParser = parser >> eof
    parseIt = parse fullParser ""
    positive str = assertEqual (Right ()) (parseIt str)
    negative str = assertBool (parseIt str |> isLeft)

testParser :: Parser a -> [String] -> [String] -> IO ()
testParser parser posStr negStr = do
  let [positive, negative] = posNegCase parser
  mapM_ positive posStr
  mapM_ negative negStr

test_parseWhitespace' :: IO ()
test_parseWhitespace' = do
  let pos = ["", " ", "\t", "\n", "\r\n", "  \r\n  ", "   \t   \r\n  \t  \n   \r  \t "]
  let neg = ["x",  " x",  "x ",  " x ",  "\t_\t"]
  testParser whiteSpaces pos neg

test_parseString :: IO ()
test_parseString = do
  let pos =  ["\"abc\"", "\"x\"", "\" \"", "\"\""]
  let neg = ["", "a", "aaa", " w ", "\"", "\"\"\""]
  testParser mString pos neg

test_parseInt :: IO ()
test_parseInt = do
  let pos = ["1", "123", "047", "11111111", "-1"]
  let neg = ["", "a1", "1a", "0w1", " 5", "4 ", "--1"]
  testParser mInt pos neg

test_parseBool :: IO ()
test_parseBool = do
  let pos = ["T", "F", "True", "False"]
  let neg = ["", " ", ""]
  testParser mBool pos neg

test_parsePush :: IO ()
test_parsePush = do
  let pos = ["@1", "@0", "@789564", "@T", "@True", "@F", "@False", "@-1"]
  let neg = ["1", "", " ", "@", "Q", "\"\""]
  testParser mPush pos neg

test_parsePop :: IO ()
test_parsePop = do
  let pos = ["$", "$1", "$999"]
  let neg = ["", "$a", "a", " "]
  testParser mPop pos neg

test_parseSpike :: IO ()
test_parseSpike = testParser mSpike ["^"] ["", "-", "a", "0"]

test_parseOrb :: IO ()
test_parseOrb = testParser mOrb ["o"] ["", "-", "a", "0"]

test_parsePocketDimensionEntrance :: IO ()
test_parsePocketDimensionEntrance = do
  let pos = ["%a", "%pS", "%pC", "%ppPPxasldjlskajrWRWEKLJRKWEj"]
  let neg = ["", "a", "%%", "%", "aaa"]
  testParser mPortalPocketDimensionEntrance pos neg

test_operatorParser :: IO ()
test_operatorParser = do
  let pos = ["<", ">", "<=", ">=", "=", "!"]
  let neg = ["", "a", "_", "aaa"]
  testParser operatorParser pos neg

test_mComparator :: IO ()
test_mComparator = do
  let pos = ["?<", "?>", "?<=", "?>=", "?=", "?!"]
  let neg = ["", "??", "?a", "?_", "a", "0", "aaa"]
  testParser mComparator pos neg

test_mLever :: IO ()
test_mLever = do
  let pos = ["/", "/?", "/abc", "\\", "\\?", "\\abc"]
  let neg = ["//", "\\\\", "", "_", "?", "abc"]
  testParser mLever pos neg

test_mComment :: IO ()
test_mComment = do
  let pos = ["`abc", "`", "``", "`\n"]
  let neg = ["", "x `","\n", "\n`"]
  testParser mComment pos neg

test_mPortalTwoWay :: IO ()
test_mPortalTwoWay = do
  let pos = ["-A", "-abc", "-c"]
  let neg = ["-", "", "- ", "--", "-1", "abc", "_A", "~A"]
  testParser mPortalTwoWay pos neg

test_mPortalEntrance :: IO ()
test_mPortalEntrance = do
  let pos = ["_A", "_abc", "_c"]
  let neg = ["_", "", "_ ", "__", "_1", "abc", "-A", "~A"]
  testParser mPortalEntrance pos neg

test_mPortalExit :: IO ()
test_mPortalExit = do
  let pos = ["~A", "~abc", "~c"]
  let neg = ["~", "", "~ ", "~~", "~1", "abc", "-A", "_A"]
  testParser mPortalExit pos neg

test_mDuplicateNItems :: IO ()
test_mDuplicateNItems = do
  let pos = ["=", "={", "=}", "=3", "=2{1", "={1"]
  let neg = ["==", "{", "}=", "", " =", "1=", "={{", "=-1", "={-1"]
  testParser mDuplicateNItems pos neg

test_mMoveStackIndex :: IO ()
test_mMoveStackIndex = do
  let pos = ["}", "{", "{2", "}3"]
  let neg = ["", "}}", "{{", "{-1"]
  testParser mMoveStackIndex pos neg

test_mOperation :: IO ()
test_mOperation = do
  let pos = [":+", ":-", ":!", ":*", ":^", ":/", ":\\", ":\\\\"]
  let neg = ["", "::", "+", "\\"]
  testParser mOperation pos neg

test_mPushWholeStack :: IO ()
test_mPushWholeStack = do
  let pos = ["=={", "==}", "==}2", "=={2"]
  let neg = ["", "=", "==", "==}-1"]
  testParser mPushWholeStack pos neg

test_mPortalPocketDimensionStart :: IO ()
test_mPortalPocketDimensionStart = do
  let pos = ["~%f", "~%Fn1"]
  let neg = ["", "%f", "~%", "~f"]
  testParser mPortalPocketDimensionStart pos neg

test_mPortalPocketDimensionEnd :: IO ()
test_mPortalPocketDimensionEnd = do
  let pos = ["_%"]
  let neg = ["", "_", "%"]
  testParser mPortalPocketDimensionEnd pos neg

{-
test_ = do
  let pos = [""]
  let neg = [""]
  testParser _ pos neg
-}
